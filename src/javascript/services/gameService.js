class GameService {
  fight(fighterA, fighterB) {
    // if (Math.random() > 0.5) {
    //     [ fighterA, fighterB ] = [ fighterB, fighterA ];
    // }
    const hitValue = fighterB.getHitPower();
    const blockValue = fighterA.getBlockPower();
    const hitDamage = hitValue > blockValue ? hitValue - blockValue : 0;

    console.log('fighterA.health >>', fighterA.name, fighterA.health);
    console.log('fighterB.health >>', fighterB.name, fighterB.health);

    fighterA.health -= hitDamage;

    return fighterA.health <= 0 ? fighterB.name : this.fight(fighterB, fighterA);
  }
}

export const gameService = new GameService();