import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    return await this._getApiData('fighters.json');
  }

  async getFighterDetails(_id) {
    const endpoint = `details/fighter/${_id}.json`;
    return await this._getApiData(endpoint);
  }

  async _getApiData(endpoint) {
    try {
      const { content } = await callApi(endpoint, 'GET');

      return JSON.parse(atob(content));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
