export default class Fighter {
    constructor(options) {
        this.name = options.name;
        this.health = options.health;
        this.attack = options.attack;
        this.defense = options.defense;
    }

    getHitPower() {
        const criticalHitChance = this._randomChance(1, 2);
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = this._randomChance(1, 2);
        return this.defense * dodgeChance;
    }

    _randomChance(from, to) {
        return from + Math.random() * (to - from);
    }
}