import View from './view';

class FighterModalView {
    constructor(fighterMetadata, handleDetailsInput) {
        this.showFighterModal(fighterMetadata, handleDetailsInput);
    }

    modal = document.querySelector('#fighter-modal');

    showFighterModal(fighterMetadata, handleDetailsInput) {
        const { _id: fighterId } = fighterMetadata;

        Object.keys(fighterMetadata).forEach(attribute => {
            const inputElement = this.modal.querySelector(`input[name=${attribute}]`);
            if (inputElement) {
                inputElement.value = fighterMetadata[attribute];
            }
        });

        const inputHandler = event => handleDetailsInput(event, fighterId);

        this.modal.addEventListener('input', inputHandler);
        this.modal.querySelector('.close-modal-btn')
            .addEventListener('click', () => {
                this.modal.close();
                this.modal.removeEventListener('input', inputHandler)
            });
        this.modal.show();
    }
}

export default FighterModalView;