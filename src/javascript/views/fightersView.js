import View from './view';
import FighterView from './fighterView';
import FighterModalView from "./fighterModalView";
import { fighterService } from "../services/fightersService";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleDetailsInput = this.handleFighterDetailsInput.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    const { _id: fighterId } = fighter;
    let fighterMetadata;

    if (this.fightersDetailsMap.has(fighterId)) {
      fighterMetadata = this.fightersDetailsMap.get(fighterId);
    } else {
      fighterMetadata = await fighterService.getFighterDetails(fighterId);
      this.fightersDetailsMap.set(fighterId, fighterMetadata);
    }

    new FighterModalView(fighterMetadata, this.handleDetailsInput);
  }

  handleFighterDetailsInput(event, fighterId) {
    const { name, value } = event.target;

    const fighterMetadata = this.fightersDetailsMap.get(fighterId);
    this.fightersDetailsMap.set(fighterId, {
       ...fighterMetadata,
       [name]: Number.isNaN(+value) ? value : +value
    })
  }
}

export default FightersView;